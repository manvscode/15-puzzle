/* Coded by Joe Marrero, October 10, 2012.
 * http://www.manvscode.com/
 */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <libcsearch/csearch.h>
#include <libcsearch/heuristics.h>
#include <libcollections/vector.h>
#include <libcollections/hash-functions.h>

#define BOARD_WIDTH   4
#define BOARD_HEIGHT  4




const int GOAL_STATE[] = {
	 1,  2,  3,  4,
	 5,  6,  7,  8,
	 9, 10, 11, 12,
	13, 14, 15,  0
};

/*
 * A collection of all of the game board
 * states that was a possibility.
 */
pvector_t states;

static void randomize_board              ( int board[], size_t width, size_t height, bool only_solvable );
static int* create_state                 ( int *board, size_t size, size_t index, size_t move_index );
static void draw_board                   ( int step, const int *board );
static void get_possible_moves           ( const void* restrict state, successors_t* restrict p_successors );
static int  heuristic_manhattan_distance ( const void* restrict state1, const void* restrict state2 );
static int  cost                         ( const void* restrict state1, const void* restrict state2 );
static int  board_compare                ( const void* restrict state1, const void* restrict state2 );

static inline size_t number_of_inversions( size_t index, const int* restrict board )
{
	size_t number_of_inversions = 0;
	size_t len = BOARD_WIDTH * BOARD_HEIGHT;

	int pivot = board[ index ];


	for( int i = index + 1; i < len; i++ )
	{
		if( board[ i ] != 0 && board[ i ] < pivot )
		{
			number_of_inversions++;
		}
	}

	return number_of_inversions;
}

static inline size_t parity( const int* restrict board )
{
	size_t count = 0;
	size_t missing_tile_row = 0;

	for( int j = 0; j < BOARD_HEIGHT; j++ )
	{
		for( int i = 0; i < BOARD_WIDTH; i++ )
		{
			size_t index = j * BOARD_WIDTH + i;

			if( board[ index ] == 0 )
			{
				missing_tile_row = j + 1;
			}

			count += number_of_inversions( index, board );
		}
	}

	return count + missing_tile_row;
}

int main( int argc, char *argv[] )
{
	srand( time(NULL) );
	pvector_create( &states, 100, malloc, free );
	astar_t* p_astar = astar_create( board_compare, pointer_hash, heuristic_manhattan_distance, cost, get_possible_moves );

	/* Produce a solvable random board */
	int* initial_state = (int*) malloc( sizeof(int) * BOARD_WIDTH * BOARD_HEIGHT );
	randomize_board( initial_state, BOARD_WIDTH, BOARD_HEIGHT, true );
	pvector_push( &states, initial_state );

	size_t p = parity( initial_state );

	printf( "parity = %ld\n", p );

	if( astar_find( p_astar, GOAL_STATE, initial_state ) )
	{
		int step = 0;
		for( astar_node_t* p_node = astar_first_node( p_astar );
			 p_node != NULL;
			 p_node = astar_next_node( p_node ) )
		{
			const int* board = astar_state( p_node );

			draw_board( step, board );
			printf("\n");
			
			step++;
		}

		astar_cleanup( p_astar );
	}
	else
	{
		printf( "No solution found for:\n\n" );
		draw_board( 0, initial_state );
		/* No solution found. */
	}
	
	astar_destroy( &p_astar );

	/* Release memory of all the possible states. */
	while( pvector_size( &states ) > 0 )
	{
		free( pvector_peek( &states ) );
		pvector_pop( &states );
	}

	pvector_destroy( &states );
	return 0;
}

/*
 * Generate a random game board.
 */
void randomize_board( int board[], size_t width, size_t height, bool only_solvable )
{
	if( !only_solvable )
	{
		/* This may or may not produce a board with a solution. */
		size_t len = width * height;
		int *numbers = (int*) malloc( sizeof(int) * len );

		for( int i = 0; i < len; i++ )
		{
			numbers[ i ] = i;
		}

		for( int i = len - 1; i >= 0; i-- )
		{
			size_t random_index = rand() % (i + 1);
			int chosen = numbers[ random_index ];

			numbers[ random_index ] = numbers[ i ];
			board[ i ] = chosen;
		}

		free( numbers );
	}
	else
	{
		/* Start with the goal */
		int current_board[ width * height ];
		memcpy( current_board, GOAL_STATE, sizeof(int) * width * height );

		int potential_moves[ 4 ][ 2 ] = {
			{ -1,  0 },
			{  1,  0 },
			{  0, -1 },
			{  0,  1 },
		};

		/* Attempt to make BOARD_HEIGHT * BOARD_WIDTH * 2 moves to scramble the goal state.
		 * This new state will be the start state.
		 */
		int moves =  BOARD_HEIGHT * BOARD_WIDTH * 2;
		while( moves-- > 0 )
		{
			for( int y = 0; y < height; y++ )
			{
				for( int x = 0; x < width; x++ )
				{
					size_t index = width * y + x;

					/* loop until you find the empty space to make moves */
					if( current_board[ index ] == 0 )
					{
						bool moved = false;

						while( !moved )
						{
							size_t m = rand() % 4;
							int move_x = x + potential_moves[ m ][ 0 ];
							int move_y = y + potential_moves[ m ][ 1 ];

							if( move_x >= 0 && move_x < width &&
		 					    move_y >= 0 && move_y < height )
							{
								/* make the move */	
								size_t move_index = width * move_y + move_x;

								int tmp = current_board[ index ];
								current_board[ index ] = current_board[ move_index ];	
								current_board[ move_index ] = tmp;

								moved = true;
							}
						}	
					}
				}
			}
		}

		/* copy randomized board to the game board */
		memcpy( board, current_board, sizeof(int) * width * height );
	}
}

/*
 * Create a new game board state.
 */
int* create_state( int *board, size_t size, size_t index, size_t move_index )
{
	static size_t number_of_states = 0;
	int* new_board = (int*) malloc( sizeof(int) * size );

	if( new_board )
	{
		memcpy( new_board, board, sizeof(int) * size );

		int tmp = new_board[ index ];
		new_board[ index ] = new_board[ move_index ];	
		new_board[ move_index ] = tmp;

		pvector_push( &states, new_board );
	}

	number_of_states++;

	#ifdef DEBUG
	printf( "Created New State (%ld)\n", number_of_states );
	#endif

	return new_board;
}


/*
 * Draw a game board state. Step 0 implies no moves have
 * occurred and is our initial state.
 */
void draw_board( int step, const int *board )
{
	for( int y = 0; y < BOARD_HEIGHT; y++ )
	{
		if( y == 1 )
		{
			if( step == 0 )
			{
				printf( " %10s     ",  "Initial" );
			}
			else
			{
				printf( " %10s %-3d ",  "Step", step );
			}
		}
		else
		{
			printf( "                " );
		}

		for( int x = 0; x < BOARD_WIDTH; x++ )
		{
			size_t index = BOARD_WIDTH * y + x;
			int num      = board[ index ];

			if( num )
			{
				printf( "|%2d", num );
			}
			else
			{
				printf( "|  " );
			}
		}

		printf( "|\n" );
	}
}

/*
 * Given a game board state, what are all of the possible
 * game boards that can result from all of the potential
 * moves. 
 */
void get_possible_moves( const void* restrict state, successors_t* restrict p_successors )
{
	int* current_board = (int*) state;

	int potential_moves[ 4 ][ 2 ] = {
		{ -1,  0 },
		{  1,  0 },
		{  0, -1 },
		{  0,  1 },
	};

	int emptyX;
	int emptyY;
	size_t index;
	bool found_empty = false;
	for( int y = 0; !found_empty && y < BOARD_HEIGHT; y++ )
	{
		for( int x = 0; !found_empty && x < BOARD_WIDTH; x++ )
		{
			index = BOARD_WIDTH * y + x;

			/* loop until you find the empty space to make moves */
			if( current_board[ index ] == 0 )
			{
				found_empty = true;
				emptyX = x;
				emptyY = y;
			}
		}
	}

	if( found_empty )
	{
		for( int m = 0; m < 4; m++ )
		{
			int move_x = emptyX + potential_moves[ m ][ 0 ];
			int move_y = emptyY + potential_moves[ m ][ 1 ];

			if( move_x >= 0 && move_x < BOARD_WIDTH &&
				move_y >= 0 && move_y < BOARD_HEIGHT )
			{
				/* make the move */	
				size_t move_index = BOARD_WIDTH * move_y + move_x;

				int* new_state = create_state( current_board, BOARD_WIDTH * BOARD_HEIGHT, index, move_index );

				successors_push( p_successors, new_state );
			}
		}
	}
}

void get_possible_moves_better( const void* restrict state, successors_t* restrict p_successors )
{
	int* current_board = (int*) state;

	int potential_moves[ 4 ][ 2 ] = {
		{ -1,  0 },
		{  1,  0 },
		{  0, -1 },
		{  0,  1 },
	};

	int emptyX;
	int emptyY;
	size_t index;
	bool found_empty = false;
	for( int y = 0; !found_empty && y < BOARD_HEIGHT; y++ )
	{
		for( int x = 0; !found_empty && x < BOARD_WIDTH; x++ )
		{
			index = BOARD_WIDTH * y + x;

			/* loop until you find the empty space to make moves */
			if( current_board[ index ] == 0 )
			{
				found_empty = true;
				emptyX = x;
				emptyY = y;
			}
		}
	}

	if( found_empty )
	{
		for( int m = 0; m < 4; m++ )
		{
			int move_x = emptyX + potential_moves[ m ][ 0 ];
			int move_y = emptyY + potential_moves[ m ][ 1 ];

			if( move_x >= 0 && move_x < BOARD_WIDTH &&
				move_y >= 0 && move_y < BOARD_HEIGHT )
			{
				/* make the move */	
				size_t move_index = BOARD_WIDTH * move_y + move_x;

				int* new_state = create_state( current_board, BOARD_WIDTH * BOARD_HEIGHT, index, move_index );

				size_t p = parity( new_state );

				if( p % 2 == 0 )
				{
					successors_push( p_successors, new_state );
				}
				else
				{
					// new state is not solvable.
					//free( new_state );
				}
			}
		}
	}
}

/*
 * The sum of the manhattan distance of each number between
 * game boards.
 */
int heuristic_manhattan_distance( const void* restrict state1, const void* restrict state2 )
{
	int* restrict board1 = (int* restrict) state1;
	int* restrict board2 = (int* restrict) state2;
	int sum = 0;

	for( int y1 = 0; y1 < BOARD_HEIGHT; y1++ )
	{
		for( int x1 = 0; x1 < BOARD_WIDTH; x1++ )
		{
			size_t index1 = BOARD_WIDTH * y1 + x1;		

			for( int y2 = 0; y2 < BOARD_HEIGHT; y2++ )
			{
				for( int x2 = 0; x2 < BOARD_WIDTH; x2++ )
				{
					size_t index2 = BOARD_WIDTH * y2 + x2;		

					if( board1[ index1 ] == board2[ index2 ] )
					{
						sum += abs( x2 - x1 ) + abs( y2 - y1 );
					}
				}
			}
		}
	}

	return sum;		
}

/*
 * The cost of making a move is 1.
 */
int cost( const void* restrict state1, const void* restrict state2 )
{
	#if 0
	int* board1 = (int*) state1;
	int* board2 = (int*) state2;
	#endif

	return 1;
}

/*
 * Two game board states are equal if every number is in the same 
 * position in both.
 */
int board_compare( const void* restrict state1, const void* restrict state2 )
{
	return memcmp( state1, state2, sizeof(int) * BOARD_WIDTH * BOARD_HEIGHT );
}
